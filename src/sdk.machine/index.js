import { Machine } from 'xstate'

export default Machine(
  {
    key: 'sdk.lms.machine',
    initial: 'initializing',
    states: {
      initializing: {
        on: {
          start: {
            target: 'idle',
            actions: 'start',
          },
        },
      },

      idle: {
        on: {
          answer: { actions: 'setInteraction' },
          location: { actions: 'setLocation' },
          complete: { actions: 'setCompleted' },
          score: { actions: 'setScore' },
          suspend: {
            actions: 'stopAsSuspend',
            target: 'stopped',
          },
          stop: {
            actions: 'stop',
            target: 'stopped',
          },
          // special transition send by DevTools
          toggleRecordSuspendData: { actions: 'setRecordSuspendData' },
        },
      },
      stopped: {},
    },
  },
)

