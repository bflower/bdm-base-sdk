import machine from './sdk.machine'
import { interpret } from 'xstate/lib/interpreter'

import { getContext, enhanceActions } from './lib/devtools'
import checkSlideshowStore from './lib/check_slideshow_store'

export default function getSdkInterpreter(sdkActions) {
  return function startSdkInterpreter(slideshowStore, DevTools) {
    checkSlideshowStore(slideshowStore)

    const interpreter = interpret(
      machine
        .withConfig({
          actions: enhanceActions(sdkActions),
        })
        .withContext(getContext(DevTools))
    )

    if (DevTools && DevTools.bindInterpreter) {
      DevTools.bindInterpreter(interpreter)
    }

    interpreter.start()

    slideshowStore.interpreter.onTransition(() => {
      interpreter.send({ type: 'location', slideshowStore })
    })

    slideshowStore.interpreter.onEvent(evObj => {
      switch (evObj.type) {
        case 'answer':
        case 'complete':
        case 'score':
          interpreter.send({
            type: evObj.type,
            slideshowStore,
          })
        return
      }
      console.warn(`bdm-base-skd - event type ${evObj.type} not handled by interpreter`)
    })

    interpreter.send({ type: 'start', slideshowStore })

    // Use both events for compatibility & LMS compliance
    window.unload = () => {
      interpreter.send({ type: 'suspend', slideshowStore })
    }
    window.onbeforeunload = () => {
      interpreter.send({ type: 'suspend', slideshowStore })
    }
  }
}
