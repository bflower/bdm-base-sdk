import assert, { util } from './assert'

export default function checkSlideshowStore(store) {
  assert(store, 'store should be an instance')
  assert(store.interpreter, 'store must have an interpreter property')
  assert(store.interpreter.onTransition, 'store must have an interpreter property with onTransition listener')
  assert(store.interpreter.onEvent, 'store must have an interpreter property with onEvent listener')
  assert(store.getSuspendData, 'store must have getSuspendData property')
  assert(util.isFunction(store.getSuspendData), 'store must have getSuspendData method')
  assert(store.getScore, 'store must have getScore property')
  assert(util.isFunction(store.getScore), 'store must have getScore method')
  assert(store.getLocation, 'store must have getLocation property')
  assert(util.isFunction(store.getLocation), 'store must have getLocation method')
  assert(store.getCurrentResponse, 'store must have getCurrentResponse property')
  assert(util.isFunction(store.getCurrentResponse), 'store must have getCurrentResponse method')
  assert(store.setExternalData, 'store must have setExternalData property')
  assert(util.isFunction(store.setExternalData), 'store must have setExternalData method')
}
