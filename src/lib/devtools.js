import { assign } from 'xstate/lib/actions'

/**
 * initialize context of interpreter if DevTool is present
 */
export function getContext(DevTools) {
  const context = {
    recordSuspendData: true,
  }
  if (DevTools && DevTools.getRecordSuspendStatus) {
    context.recordSuspendData = DevTools.getRecordSuspendStatus()
  }

  return context
}

/**
 * evolve actions interpreter to handle record suspend status
 * action assign context on execution
 */
export function enhanceActions(actions) {
  /**
   * devtools may trigger 'toggleRecordSuspendData' with event object { active }
   */
  actions.setRecordSuspendData = assign((ctx, evObj) => {
    return { recordSuspendData: evObj.active }
  })
  return actions
}
