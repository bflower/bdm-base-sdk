# bdm-base-sdk

module sdk de base permettant l&#39;intégration de wrapper de communication autour des modules b-eden MROCS.

## Install

    npm i @b-flower/bdm-base-sdk

# API

## `getSdkInterpreter(actions)`

Cette méthode doit être implémentée par un wrapper. (cf [bdm-scorm-wrapper](https://bitbucket.org/bflower/bdm-scorm-wrapper))

Retourne une fonction.

### Params

<dl>
    <dt>actions</dt>
    <dd>Objet contenant la liste des actions en réponse d'évènements</dd>
</dl>

`actions` est un objet obligatoire

### Retour de `getSdkInterpreter`

La fonction de retour est une fonction de démarrage de l'interpréteur du SDK.
Elle doit être démarrer dans le module final que vous construisez.

Elle prend comme premier paramètre un `store`.
Le `store` doit implémenter les propriétés et méthodes suivantes:

````javascript
  store.interpreter.onTransition // function
  store.interpreter.onEvent // function
  store.getSuspendData // function
  store.getScore // function
  store.getLocation // function
  store.getCurrentResponse // function
  store.setExternalData // function
````

Le deuxième paramètre est optionel et correspond au devtools [bdm-base-sdk-devtools](https://bitbucket.org/bflower/bdm-base-sdk-devtools)



### Exemple d'intégration dans le wrapper

```javascript
// index.js
import getSdkInterpreter from '@b-flower/bdm-base-sdk'
import * as actions from './actions'

const startSdkInterpreter = getSdkInterpreter(actions)

export default startSdkInterpreter
```

## Définition `actions` de `getSdkInterpreter`

L'object `actions` est un ensemble de `callback` d'évènement.

````js
- {Object} actions
- {function} actions.start - function(context, eventObject)
- {function} actions.setInteraction - function(context, eventObject)
- {function} actions.setLocation - function(context, eventObject)
- {function} actions.setCompleted - function(context, eventObject)
- {function} actions.setScore - function(context, eventObject)
- {function} actions.stopAsSuspend - function(context, eventObject)
- {function} actions.stop - function(context, eventObject)
````


Chaque propriétés de `actions` est une fonction avec la signature `function(context, eventObject)`


### Description de `context`

<dl>
    <dt>recordSuspendData=true</dt>
    <dd>indique si les <code>suspend_data</code> doivent être sauvegardées - par défaut à <code>true</code>. Uniquement utile en phase développement et en association avec le module <code>@b-flower/bdm-base-sdk-devtools</code> </dd>
</dl>

#### Exemple d'utilisation du `context`

```javascript
function saveSuspend(ctx, evObj) {
  const { slideshowStore } = evObj

  // ici recordSuspendData est commandé par le devtools et permet au développeur de réagir en fonction
  if (ctx.recordSuspendData) {
    SCORM_API.saveSuspend(JSON.stringify(slideshowStore.getSuspendData()))
  } else {
    SCORM_API.saveSuspend(JSON.stringify({}))
  }
}
```

### Description de `eventObject`

<dl>
    <dt>slideshowStore</dt>
    <dd>Voir description de <code>slideshowStore</code></dd>
</dl>

### Description de `slideshowStore`


````javascript
slideshowStore = {
  // récupère l'id de la slide en cours
  getLocation() => String
  // récupère la réponse de la slide en cours
  getCurrentResponse() => {
    id,
    description,
    learnerResponse,
    correctResponse = null,
    result = null,
    latency = null,
    weighting = null,
    objectiveId = null,
    dtmTime = new Date(),
  },
  // récupère le score du module
  getScore() => { score, scaled, min = 0, max = 100 },
  // récupère les données en suspend pour le module
  getSuspendData() => restoreData
  // informe le module d'un ensemble de données en lecture seule
  setExternalData({
    suspendData: Object,
    learnerName: String|null,
    learnerId: String|Number|null,
    language: String|null,
    maxTimeAllowed: String|Number|null,
    mode: Enum (preview, review, normal),
    // “preview”
    //    The module is presented without the intent of recording any
    //    information about the current learner session.
    // “normal”
    //    The module is presented with the intent of recording information
    //    about the current learner session.
    //    This is the default value if no mechanism is in place to identify
    //    the mode.
    // “review”
    //    The module has previously recorded information about the learner
    //    attempt and is presented without the intent of updating this
    //    information with data from the current learner session.
    credit: Enum (credit, no-credit, null),
  })
}
````

### Exemple mise en place des `actions`

Dans les exemples ci-dessous, `SCORM_API` est un facilitateur de Scorm 2004

#### `start`

````javascript
export function start(ctx, evObj) {
  const { slideshowStore } = evObj
  SCORM_API.start()

  const { slideshowStore } = evObj
  SCORM_API.start()

  const externalData = {
    learnerId: SCORM_API.get('cmi.learner_id'),
    learnerName: SCORM_API.get('cmi.learner_name'),
    language: SCORM_API.get('cmi.learner_preference.language'),
    maxTimeAllowed: SCORM_API.get('cmi.max_time_allowed'),
    mode: SCORM_API.get('cmi.mode'),
    credit: SCORM_API.get('cmi.credit'),
  }

  externalData.suspendData = getSuspendData()

  slideshowStore.setExternalData(externalData)
}

function getSuspendData(ctx, evObj) {
  try {
    return JSON.parse(SCORM_API.getSuspendData())
  } catch (e) {
    return {}
  }
}

`````

#### `stop`

````javascript
export function stop(ctx, evObj) {
  // ici SCORM_API est un facilitateur de Scorm 2004
  SCORM_API.stop()
}
````

#### `stopAsSuspend`

````javascript
export function stopAsSuspend(ctx, evObj) {
  saveSuspend(ctx, evObj)
  // ici SCORM_API est un facilitateur de Scorm 2004
  SCORM_API.stop(true)
}

function saveSuspend(ctx, evObj) {
  const { recordSuspendData } = ctx
  const { slideshowStore } = evObj
  if (recordSuspendData) {
    SCORM_API.saveSuspend(JSON.stringify(slideshowStore.getSuspendData()))
  } else {
    SCORM_API.saveSuspend(JSON.stringify({}))
  }
}
````

#### `setScore`

````javascript
export function setScore(ctx, evObj) {
  const { slideshowStore } = evObj

  const {
    score,
    scaled,
    min = 0,
    max = 100,
  } = slideshowStore.getScore()
  SCORM_API.setScore({ score, min, max, threshold })
}
````

#### `setLocation`

````javascript
export function setLocation(ctx, evObj) {
  const { slideshowStore } = evObj

  SCORM_API.setLocation(slideshowStore.getLocation())
}
````

#### `setCompleted`

````javascript
export function setCompleted() {
  SCORM_API.complete()
}
````

#### `setInteraction`

````javascript
export function setInteraction() {
  const { slideshowStore } = evObj
  const {
    id,
    description,
    learnerResponse,
    correctResponse = null,
    result,
    latency = null,
    weighting = null,
    objectiveId = null,
    dtmTime = new Date(),
  } = slideshowStore.getCurrentResponse()

  SCORM_API.interaction.setInteractionJSON(
    id, // interaction Id
    learnerResponse, // learner_response
    result, // result field
    correctResponse, // correctResponse - devrait être présent sur slide.interaction
    description, // description
    weighting, //weighting - devrait être présent sur slide.interaction
    latency, //latency
    objectiveId, // objectiveid - devrait être présent sur slide.interaction
    dtmTime //dtmTime
  )
}
````
